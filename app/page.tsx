import About from "@/components/About";
import ContactMe from "@/components/ContactMe";
import Header from "@/components/Header";
import Hero from "@/components/Hero";
import Projects from "@/components/Projects";
import WorkExperience from "@/components/WorkExperience";
import Link from "next/link";

export default function Home() {
  return (
    <main className="bg-slate-100 text-jet dark:bg-jet dark:text-silver-400 h-screen snap-y snap-mandatory overflow-y-scroll overflow-x-hidden z-0 scrollbar scrollbar-track-light-jet/20 scrollbar-thumb-caramel/80">
      <Header />

      <section id="hero" className="snap-start">
        <Hero />
      </section>

      <section id="about" className="snap-center">
        <About />
      </section>

      <section id="experience" className="snap-center">
        <WorkExperience />
      </section>

      <section id="projects" className="snap-start">
        <Projects />
      </section>

      <section id="contact" className="snap-start">
        <ContactMe />
      </section>

      {/* <Link href="#hero">
        <footer className="sticky bottom-9 w-full cursor-pointer">
          <div className="flex items-center justify-center">
            <img
              className="h-10 w-10 rounded-full filter grayscale hover:grayscale-0 cursor-pointer"
              src="https://gitlab.com/uploads/-/system/user/avatar/11589570/avatar.png"
              alt=""
            />
          </div>
        </footer>
      </Link> */}
    </main>
  );
}

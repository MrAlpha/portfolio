"use client"
import React, { useEffect, useState } from "react";
import { SocialIcon } from "react-social-icons";
import { motion } from "framer-motion";
import Link from "next/link";
import { Social } from "@/types";
import { sanityClient } from "@/utils/sanity/client";
import groq from "groq";
import { ModeToggle } from "./DarkModeToggle";

const query = groq`
  *[_type == "social"]
`;

const Header = () => {
  const [socials, setSocials] = useState<Social[] | null>(null)
  const [isLoading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (isLoading === false) setLoading(true)
      const response = await sanityClient.fetch(query)
    setSocials(response)
      } catch (error) {
        console.log(error)
      } finally {
        setLoading(false)
      }
    }

    fetchData(); // Exécuter immédiatement une fois

    const intervalId = setInterval(fetchData, 10000); // Réexécuter toutes les 10 secondes

    return () => clearInterval(intervalId); // Nettoyage de l'intervalle lors du démontage du composant

  }, [])
  return (
    <header className="sticky top-0 flex items-start xl:items-center justify-between p-5 z-20 max-w-7xl mx-auto">
      <motion.div
        initial={{
          x: -500,
          opacity: 0,
          scale: 0.5,
        }}
        animate={{
          x: 0,
          opacity: 1,
          scale: 1,
        }}
        transition={{ duration: 1.5 }}
        className="flex flex-row items-center"
      >
        {socials && socials.map((social) => (
          <SocialIcon
            key={social._id}
            url={social.url}
            fgColor="gray"
            bgColor="transparent"
          />
        ))}
      </motion.div>

     
        <motion.div
          initial={{
            x: 500,
            opacity: 0,
            scale: 0.5,
          }}
          animate={{
            x: 0,
            opacity: 1,
            scale: 1,
          }}
          transition={{ duration: 1 }}
          className="flex flex-row items-center text-gray-300 cursor-pointer"
        >
          <ModeToggle />
          <SocialIcon
            className="cursor-pointer"
            network="email"
            fgColor="gray"
            bgColor="transparent"
            href="#contact"
          />
          <Link href='#contact' className="uppercase hidden md:inline-flex text-sm text-gray-400">
            Get In Touch
          </Link>
        </motion.div>
      
    </header>
  );
};

export default Header;

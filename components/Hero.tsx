"use client";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { Cursor, useTypewriter } from "react-simple-typewriter";
import BackgroundCircles from "./BackgroundCircles";
import { sanityClient, urlFor } from "@/utils/sanity/client";
import groq from "groq";
import { PageInfo } from "@/types";
import Image from "next/image";

const pageInfoQuery = groq`
  *[_type == "pageInfo"][0]
`;

function Hero() {
  const [pageInfo, setPageInfo] = useState<PageInfo | null>(null);
  const [isLoading, setLoading] = useState(true);
  useEffect(() => {
    sanityClient
      .fetch(pageInfoQuery, {
        revalidate: 10,
      })
      .then((data) => {
        if (pageInfo !== null) setPageInfo(null);
        if (isLoading === false) setLoading(true);
        setPageInfo(data);
      })
      .catch((err) => console.log(err))
      .finally(() => setLoading(false));
  }, []);
  const [text, count] = useTypewriter({
    words: [
      `Hi, my name is ${pageInfo?.name}`,
      "Guy-who-loves-Coffee.tsx",
      "<ButLovesToCodeMore />",
    ],
    loop: true,
    delaySpeed: 2000,
  });
  return (
    <div className="h-screen flex flex-col space-y-8 items-center justify-center text-center overflow-hidden">
      <BackgroundCircles />
      {isLoading === false ? (
        <>
          {pageInfo !== null ? (
            <Image
              src={urlFor(pageInfo.heroImage).url()}
              alt="profile_picture"
              className="relative rounded-full h-32 w-32 mx-auto object-cover"
              loading="lazy"
              width={128}
              height={128}
            />
          ) : (
            <div className="relative rounded-full h-32 w-32 mx-auto bg-slate-400/80 animate-pulse"></div>
          )}
        </>
      ) : (
        <div className="relative rounded-full h-32 w-32 mx-auto bg-slate-400/80 animate-pulse"></div>
      )}
      <div className="z-20">
        <h2 className="text-xs md:text-sm uppercase text-[#957F75] pb-2 tracking-[15px]">
          {pageInfo?.role}
        </h2>
        <h1 className="text-2xl md:text-5xl lg:text-6xl font-semibold px-10">
          <span className="mr-3">{text}</span>
          <Cursor cursorColor="#FFA586" />
        </h1>

        <div className="pt-5">
          <Link href="#about">
            <button className="heroButton">About</button>
          </Link>
          <Link href="#experience">
            <button className="heroButton">Experience</button>
          </Link>
          {/* <Link href="#skills">
            <button className="heroButton">Skills</button>
          </Link> */}
          <Link href="#projects">
            <button className="heroButton">Projects</button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Hero;

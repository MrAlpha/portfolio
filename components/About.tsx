"use client";
import { PageInfo } from "@/types";
import { motion } from "framer-motion";
import { sanityClient, urlFor } from "@/utils/sanity/client";
import { groq } from "next-sanity";
import React, { useEffect, useState } from "react";

const pageInfoQuery = groq`
  *[_type == "pageInfo"][0]
`;

type Props = {};

function About({}: Props) {
  const [pageInfo, setPageInfo] = useState<PageInfo | null>(null);
  const [isLoading, setLoading] = useState(true);
  useEffect(() => {
    const fetchData = async () => {
      try {
        if (isLoading === false) setLoading(true);
        const response = await sanityClient.fetch(pageInfoQuery);
        setPageInfo(response);
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData(); // Exécuter immédiatement une fois

    const intervalId = setInterval(fetchData, 10000); // Réexécuter toutes les 10 secondes

    return () => clearInterval(intervalId); // Nettoyage de l'intervalle lors du démontage du composant
  }, []);
  return (
    <motion.div
      initial={{
        opacity: 0,
      }}
      whileInView={{
        opacity: 1,
      }}
      transition={{ duration: 1.5 }}
      viewport={{ once: true }}
      className="flex flex-col relative max-w-7xl justify-evenly mx-auto items-center h-screen"
    >
      <h3 className="mt-16 uppercase tracking-[20px] text-silver-600 text-xl md:text-2xl">
        About
      </h3>
      <div className="flex flex-col md:flex-row text-center md:text-left max-w-7xl px-16 justify-evenly mx-auto items-center w-full">
        <motion.img
          initial={{
            x: -200,
            opacity: 0,
          }}
          transition={{
            duration: 1.2,
          }}
          whileInView={{ x: 0, opacity: 1 }}
          viewport={{ once: true }}
          src={pageInfo ? urlFor(pageInfo?.profilePic).url() : ""}
          alt="me_picture"
          className="mb-3 md:mb-0 flex-shrink-0 w-32 h-32 rounded-full object-cover md:rounded-lg md:w-64 md:h-96 xl:w-[500px] xl:h-[600px]"
        />

        <div className="space-y-10 px-0 md:px-10">
          <h4 className="text-xl md:text-4xl font-semibold">
            Here is a{" "}
            <span className="underline decoration-caramel">little</span>{" "}
            background:
          </h4>
          <p className=" text-xs md:text-sm">
            {pageInfo?.backgroundInformation}
          </p>
        </div>
      </div>
    </motion.div>
  );
}

export default About;

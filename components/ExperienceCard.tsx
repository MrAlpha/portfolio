import { Experience } from "@/types";
import { urlFor } from "@/utils/sanity/client";
import { motion } from "framer-motion";
import Image from "next/image";

type Props = {
  experience: Experience;
};

function ExperienceCard({ experience }: Props) {
  return (
    <article className="flex flex-col rounded-lg items-center space-y-4 md:space-y-7 flex-shrink-0 w-[350px] md:w-[600px] xl:w-[900px] snap-center bg-silver-500 dark:bg-light-jet p-6 md:p-10 md:opacity-40 md:hover:opacity-100 cursor-pointer transition-opacity duration-200 overflow-hidden">
      <motion.img
        initial={{
          y: -100,
          opacity: 0,
        }}
        transition={{ duration: 1.2 }}
        whileInView={{ opacity: 1, y: 0 }}
        viewport={{ once: true }}
        src={urlFor(experience?.companyImage).url()}
        alt={`logo-${experience?.company}`}
        className="w-20 h-20 md:w-32 md:h-32 rounded-full md:rounded-full xl:w-[200px] xl:h-[200px] object-cover object-center"
      />
      <div className="px-0 md:px-10">
        <h4 className="text-xl md:text-4xl font-light">
          {experience?.jobTitle}
        </h4>
        <p className="font-bold text-base md:text-2xl mt-1">
          {experience?.company}
        </p>
        <div className="flex space-x-2 my-2">
          {experience?.technologies.map((tech) => {
            return (
              <Image
                key={tech._id}
                className="h-6 w-6 md:h-10 md:w-10 rounded-full"
                src={urlFor(tech.image).url()}
                alt={`logo-tech-${tech.title}`}
                width={20}
                height={20}
              />
            );
          })}
        </div>
        <p className="uppercase py-5 text-light-jet dark:text-silver-500 text-sm">
          {new Date(experience.dateStarted).toDateString()} -{" "}
          {experience.isCurrentlyWorkingHere
            ? "Present"
            : new Date(experience.dateEnded).toDateString()}
        </p>
        <ul className="list-disc space-y-2 md:space-y-4 ml-5 text-xs md:text-lg">
          {experience.points.map((point, i) => (
            <li key={i}>{point}</li>
          ))}
        </ul>
      </div>
    </article>
  );
}

export default ExperienceCard;

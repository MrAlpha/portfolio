"use client";

import { PageInfo } from "@/types";
import { sanityClient } from "@/utils/sanity/client";
import { groq } from "next-sanity";
import { useEffect, useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { motion } from "framer-motion";
import { PhoneIcon, MapPinIcon, EnvelopeIcon } from "@heroicons/react/24/solid";

const pageInfoQuery = groq`
  *[_type == "pageInfo"][0]
`;

type Inputs = {
  name: string;
  email: string;
  subject: string;
  message: string;
};

type Props = {};

function ContactMe({}: Props) {
  const [pageInfo, setPageInfo] = useState<PageInfo | null>(null);
  const [isLoading, setLoading] = useState(true);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    if (pageInfo === null) return;
    window.location.href = `mailto:${pageInfo.email}?subject=${data.subject}&body=Hi, my name is ${data.name}. ${data.message}`;
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (isLoading === false) setLoading(true);
        const response = await sanityClient.fetch(pageInfoQuery);
        setPageInfo(response);
      } catch (error) {
        console.log("Erreur lors de la récupération des données:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();

    const intervalId = setInterval(fetchData, 10000); // Réexécuter toutes les 10 secondes

    return () => clearInterval(intervalId); // Nettoyage de l'intervalle lors du démontage du composant
  }, []);
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
      viewport={{ once: true }}
      className="flex relative flex-col text-center md:text-left max-w-7xl px-10 space-y-6 h-screen justify-evenly mx-auto items-center"
    >
      <h3 className="mt-16 uppercase tracking-[20px] text-silver-600 text-xl md:text-2xl">
        Contact
      </h3>

      <div className="flex flex-col space-y-10 items-center">
        <h4 className="text-base sm:text-xl md:text-4xl font-semibold text-center">
          I have got just what you need.{" "}
          <span className="decoration-caramel/750 underline">Lets Talk.</span>
        </h4>

        {pageInfo && (
          <div className="space-y-4 md:space-y-10">
            <div className="flex items-center space-x-5 justify-center">
              <PhoneIcon className="text-caramel h-5 md:h-7 w-5 md:w-7 animate-pulse" />
              <p className="text-base md:text-xl">{pageInfo.phoneNumber}</p>
            </div>
            <div className="flex items-center space-x-5 justify-center">
              <EnvelopeIcon className="text-caramel h-5 md:h-7 w-5 md:w-7 animate-pulse" />
              <a
                className="text-base md:text-xl"
                href={`mailto:${pageInfo.email}`}
              >
                {pageInfo.email}
              </a>
            </div>
            <div className="flex items-center space-x-5 justify-center">
              <MapPinIcon className="text-caramel h-5 md:h-7 w-5 md:w-7 animate-pulse" />
              <p className="text-base md:text-xl">{pageInfo.address}</p>
            </div>
          </div>
        )}

        <form
          onSubmit={handleSubmit(onSubmit)}
          className="flex flex-col space-y-2 w-[90%] mx-8 sm:w-fit sm:mx-auto"
        >
          <div className="flex space-x-2">
            <input
              className="contactInput w-[50%]"
              placeholder="Name"
              type="text"
              {...register("name")}
            />
            <input
              className="contactInput w-[50%]"
              placeholder="Email"
              type="text"
              {...register("email")}
            />
          </div>
          <input
            className="contactInput"
            placeholder="Subject"
            type="text"
            {...register("subject")}
          />
          <textarea
            className="contactInput h-36"
            placeholder="Message"
            {...register("message")}
          />

          <button
            type="submit"
            className="
      bg-caramel py-3 md:py-5 px-10 rounded-md text-black font-bold text-base md:text-lg "
          >
            Submit
          </button>
        </form>
      </div>
    </motion.div>
  );
}

export default ContactMe;

"use client";
import React, { useEffect, useState } from "react";
import { motion } from "framer-motion";
import ExperienceCard from "./ExperienceCard";
import { Experience } from "@/types";
import { groq } from "next-sanity";
import { sanityClient } from "@/utils/sanity/client";

const query = groq`
  *[_type == "experience"] {
    ...,
    technologies[]->
  }
`;

function WorkExperience() {
  const [experiences, setExperiences] = useState<Experience[] | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (loading === false) setLoading(true);
        const response = await sanityClient.fetch(query);
        setExperiences(response);
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData(); // Exécuter immédiatement une fois

    const intervalId = setInterval(fetchData, 10000); // Réexécuter toutes les 10 secondes

    return () => clearInterval(intervalId); // Nettoyage de l'intervalle lors du démontage du composant
  }, []);
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
      viewport={{ once: true }}
      className="relative h-screen flex overflow-hidden flex-col text-left pb-14  max-w-full px-10 justify-evenly mx-auto items-center"
    >
      <h3 className="mt-16 uppercase tracking-[20px] text-silver-600 text-xl md:text-2xl">
        Experience
      </h3>

      <div className="w-[95vw] md:w-full flex space-x-5 overflow-x-scroll p-10 snap-x snap-mandatory scrollbar scrollbar-track-light-jet/20 scrollbar-thumb-caramel/80">
        {experiences?.map((exp) => {
          return <ExperienceCard key={exp._id} experience={exp} />;
        })}
      </div>
    </motion.div>
  );
}

export default WorkExperience;

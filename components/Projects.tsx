"use client";
import React, { useEffect, useState } from "react";
import { motion } from "framer-motion";

import Link from "next/link";
import { Project } from "@/types";
import { sanityClient, urlFor } from "@/utils/sanity/client";
import { groq } from "next-sanity";
import Image from "next/image";
import { Button } from "./ui/button";

const query = groq`
  *[_type == "project"] {
    ...,
    technologies[]->
  }
`;

function Projects() {
  const [projects, setProjects] = useState<Project[] | null>(null);
  const [isLoading, setLoading] = useState(true);
  useEffect(() => {
    const fetchData = async () => {
      try {
        if (isLoading === false) setLoading(true);
        const response = await sanityClient.fetch(query);
        setProjects(response);
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData(); // Exécuter immédiatement une fois

    const intervalId = setInterval(fetchData, 10000); // Réexécuter toutes les 10 secondes

    return () => clearInterval(intervalId); // Nettoyage de l'intervalle lors du démontage du composant
  }, []);
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
      viewport={{ once: true }}
      className="flex relative overflow-hidden flex-col text-left max-w-full h-screen justify-center mx-auto items-center z-0 "
    >
      <h3 className="mt-20 uppercase tracking-[20px] text-silver-600 animate-pulse text-xl md:text-2xl mb-0 pb-0">
        Projects
      </h3>
      <div className="relative w-full flex overflow-x-scroll overflow-y-hidden snap-x snap-mandatory z-20 scrollbar scrollbar-track-light-jet/20 scrollbar-thumb-caramel/80 lg:space-y-4 pb-5">
        {projects &&
          projects.map((project, i) => (
            <motion.div
              key={project._id}
              className="w-screen flex flex-col space-y-5 items-center flex-shrink-0 snap-center px-8 pb-11 md:px-44 md:pb-52 lg:pt-12 h-screen"
            >
              <Image
                src={urlFor(project.image).url()}
                alt={`${project.title} logo`}
                className="md:mb-0 flex-shrink-0 w-56 h-56 rounded-full md:rounded-lg md:w-64 md:h-96 xl:w-[500px] xl:h-[400px] object-contain cursor-pointer"
                width={224}
                height={224}
              />

              <Button variant="ghost">
                <Link href={project.linkToBuild}>Go To Build</Link>
              </Button>

              <div className="space-y-4 md:space-y-10 px-0 md:px-10 max-w-6xl">
                <h4 className="text-2xl md:text-4xl font-semibold text-center">
                  <span className="decoration-[#F7AB0A]/50 underline">
                    Case Study {i + 1} of {projects.length}:
                  </span>{" "}
                  {project.title}
                </h4>

                <p className="text-xs md:text-lg text-center md:text-left">
                  {project.summary}
                </p>
              </div>
            </motion.div>
          ))}
      </div>
      <div className="w-full absolute top-[30%] bg-[#F7AB0A]/10 left-0 h-[500px] -skew-y-12" />
    </motion.div>
  );
}

export default Projects;

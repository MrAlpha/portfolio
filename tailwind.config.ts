import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: {
      "Penn-red": "#92140C",
      "caramel": "#BE7C4D",
      "jet": "#353238",
      "light-jet": "#47434C",
      "jasper": "#BE5A38",
      "silver": {
        "400": "#C1B4AE",
        "500": "#A6948C",
        "600": "#7F6A62",
      },
    },
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
  },
  plugins: [require("tailwind-scrollbar")],
};
export default config;
